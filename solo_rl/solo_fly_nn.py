from collections import OrderedDict

from torch import nn


class SoloFlyNet(nn.Module):
    def __init__(self, dim):
        super(SoloFlyNet, self).__init__()
        layers = OrderedDict()
        for i in range(len(dim) - 1):
            layers[f'L{i}'] = nn.Linear(dim[i], dim[i + 1])
            layers[f'A{i}'] = nn.Tanh()
        self.__model = nn.Sequential(layers)

    def forward(self, x):
        return self.__model(x)


def main():
    pass


if __name__ == '__main__':
    main()
