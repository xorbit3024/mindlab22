from collections import defaultdict
from math import exp
from random import random, choice, randint

import numpy as np
import torch as t
from matplotlib import pyplot as plt
from torch import optim, nn

from mind.agents import World, Agent
from solo_rl.solo_fly_nn import SoloFlyNet


def main():
    t.set_default_tensor_type(t.cuda.FloatTensor)

    world = SoloFlyWorld()
    agent = SoloFlyAgent()

    xs, ys = [], []
    als, lls = [], []
    for i in range(3000):
        x, y, vx = world.get_state(0, 3)

        xs.append(x)
        ys.append(y)

        if agent.losses() is not None:
            al, ll = agent.losses()
            als.append(al)
            lls.append(ll)

        dx, dy = agent.react((x, y, vx))
        dy = max(dy, 0)
        x += vx + dx
        y += randint(-1, 0) + dy
        vx = randint(-1, 1)
        world.set_state(0, 3, (x, y, vx))

    plt.plot(xs, label='x')
    plt.plot(ys, label='y')
    # plt.plot(als, label='action loss')
    # plt.plot(lls, label='loss loss')
    plt.legend()
    plt.show()


class SoloFlyWorld(World):
    def __init__(self, size=3):
        super(SoloFlyWorld, self).__init__(size)


class SoloFlyAgent(Agent):
    def __init__(self, size=2, max_states=5):
        super(SoloFlyAgent, self).__init__(0, size)

        if not t.cuda.is_available():
            raise RuntimeError('No CUDA found.')

        self.__states = []
        self.__max_states = max_states

        self.__action_net = SoloFlyNet([3 + size * self.__max_states, 8, 8, 4, 2]).to('cuda')
        self.__action_opt = optim.Adam(self.__action_net.parameters())
        self.__action_loss = None

        self.__aloss_net = SoloFlyNet([2 + 3 + size * self.__max_states, 8, 8, 4, 2, 1]).to('cuda')
        self.__aloss_opt = optim.SGD(self.__aloss_net.parameters(), .3)
        self.__aloss_loss_fn = nn.MSELoss()
        self.__aloss_loss = None

    def __loss(self, x):
        # min mse over remembered states
        return min((t.mean((x - s) ** 2, 0, True) for s in self.__states), key=lambda l: l.item(), default=0)

    def react(self, observation):
        if len(self.__states) < self.__max_states:
            self.__states.append(t.tensor(observation[self._begin:self._end], dtype=t.float32).to('cuda'))
            return 0, 0

        if self.__action_loss is not None:
            loss = self.__loss(t.tensor(observation[self._begin:self._end], dtype=t.float32).to('cuda'))
            self.__aloss_loss = self.__aloss_loss_fn(self.__action_loss, loss)
            self.__aloss_loss.backward()
            self.__aloss_opt.step()

        self.__action_opt.zero_grad()
        x = t.cat(self.__states + [t.tensor(observation, dtype=t.float32)]).to('cuda')
        action = self.__action_net(x)
        action_loss = self.__aloss_net(t.cat((x, action)))
        action_loss.backward()
        self.__action_opt.step()

        self.__aloss_opt.zero_grad()
        action = action.clone().detach().requires_grad_(False)
        self.__action_loss = self.__aloss_net(t.cat((x, action)))

        self.__states.pop(0)
        self.__states.append(t.tensor(observation[self._begin:self._end], dtype=t.float32).to('cuda'))

        return action.tolist()

    def losses(self):
        if self.__action_loss is not None and self.__aloss_loss is not None:
            return self.__action_loss.item(), self.__aloss_loss.item()
        else:
            return None


if __name__ == '__main__':
    main()
