class World:
    def __init__(self, size):
        self._env = [0] * size

    def get_state(self, begin, end):
        return self._env[begin:end]

    def set_state(self, begin, end, data):
        self._env[begin:end] = data


class Agent:
    def __init__(self, begin, end):
        self._begin = begin
        self._end = end

    def react(self, observation):
        pass


def main():
    pass


if __name__ == '__main__':
    main()
